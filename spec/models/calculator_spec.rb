require 'rails_helper'

RSpec.describe Calculator do
  let! (:item1) { FactoryGirl.create(:item) }
  let! (:item2) { FactoryGirl.create(:item, quantity: 2, unitary_amount: 20.000, vat: 21) }

  it 'calculates invoice' do
    result = Calculator.new(Item.all, [:total_amount_without_taxes, :vat, :total_amount]).calculate_invoice
    expect(result[:total_amount_without_taxes]).to eq 50
    expect(result[:vat]).to eq 9.4
    expect(result[:total_amount]).to eq 59.4
  end
end
