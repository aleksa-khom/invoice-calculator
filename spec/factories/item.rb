FactoryGirl.define do
  factory :item do
    article        { Faker::Commerce.product_name }
    quantity       1
    unitary_amount 10.000
    vat            10
  end
end
