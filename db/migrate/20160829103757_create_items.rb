class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :article
      t.integer :quantity
      t.decimal :unitary_amount, precision: 4, scale: 2
      t.integer :vat

      t.timestamps null: false
    end
  end
end
