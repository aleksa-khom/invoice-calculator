class Item < ActiveRecord::Base
  class << self
    def total_invoice
      print_result(Item.all, [:total_amount_without_taxes, :vat, :total_amount])
    end

    def total_grouped_invoice
      Item.all.group_by(&:vat).each do |vat, items|
        puts "For group with VAT #{vat}:"
        print_result(items, [:total_amount_without_taxes, :vat, :total_amount])
      end
    end

    private

    def print_result(items, options)
      calculator = Calculator.new(items, options)
      calculator.calculate_invoice.each { |key, value| puts "#{key}: #{value.round(2)} €" }
    end
  end
end
