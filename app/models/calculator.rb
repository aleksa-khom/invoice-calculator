class Calculator
  def initialize(items, options)
    @items = items
    @options = options
  end

  def calculate_invoice
    @options.each_with_object({}) { |value, result| result[value] = self.send(value) }
  end

  private

  def total_amount_without_taxes
    @items.inject(0){ |sum, item| sum + item.unitary_amount * item.quantity }
  end

  def vat
    @items.inject(0){ |sum, item| sum + item.unitary_amount * item.quantity * vat_amount(item) }
  end

  def total_amount
    total_amount_without_taxes + vat
  end

  def vat_amount(item)
    item.vat / 100.0
  end
end
